## Purpose of this Repository

This repository compromises the compiled product information of:
**S2-0031 Placer P**

## Reference links:
[mikromontage.shop](https://mikromontage.shop/en/product/)

## File Description

|file|description|
|:----|:----|
|dxp_general_description.md|overview of the product|
|dxp_technical_specification.md|summary of the main technical data of the product|
|dpx_download_data.md|collection of the product downloadable data|

The above listed files are describing the product as an overview and are ment to be automatically implemented in different websites like the mikromontage.shop.

<!-- Copyright &copy; Häcker Automation GmbH -->
