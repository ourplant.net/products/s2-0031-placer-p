Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0031-placer-p).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0031-placer-p/-/raw/main/01_operating_manual/S2-0031_B_Betriebsanleitung.pdf)|
|assembly drawing           |[de/en](https://gitlab.com/ourplant.net/products/s2-0031-placer-p/-/raw/main/02_assembly_drawing/s2-0031_A_ZNB_placer.pdf)|
|circuit diagram            |[de/en](https://gitlab.com/ourplant.net/products/s2-0031-placer-p/-/raw/main/03_circuit_diagram/S2-0031_Placer-D.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0031-placer-p/-/raw/main/04_maintenance_instructions/S2-0031_A_Wartungsanweisungen.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0031-placer-p/-/raw/main/05_spare_parts/S2-0031_B_EVL_Placer.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
